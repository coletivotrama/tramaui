'use script';

var tramaApp = angular.module('tramaApp', []);



tramaApp.controller('EventsListCtrl',function($scope){
  $scope.events=[
    {'title':'Trama 1ano',
    'data':'13/02',
    'horario':'08:00',
    'local':'Avenida.....',
    'description':'Venha comemorar conosco o nosso primeiro aniversario!!',
    'image':'img/evento3.jpg'},

    {'title':'Trama 2anos',
    'data':'13/03',
    'horario':'09:00',
    'local':'Rua boladona.....',
    'description':'dasdasdasdaxvxv',
    'image':'img/evento3.jpg'},

    {'title':'Trama 3anos',
    'data':'13/04',
    'horario':'10:00',
    'local':'Alameda.....',
    'description':'dsdads!!',
    'image':'img/evento3.jpg'},

    {'title':'Trama 4anos',
    'data':'13/04',
    'horario':'10:00',
    'local':'Alameda.....',
    'description':'dsdads!!',
    'image':'img/evento3.jpg'},

    {'title':'Trama 5anos',
    'data':'13/04',
    'horario':'10:00',
    'local':'Alameda.....',
    'description':'dsdads!!',
    'image':'img/evento3.jpg'}
  ]

  // $scope.participacao=[
  //   {'oquevailevar':'Oficina de malabares', 'quem':'Bernardo'},
  //   {'oquevailevar':'Oficina de skate','quem':'Salazar'},
  //   {'oquevailevar':'Oficina de teatro', 'quem':'Gabriela'},
  //   {'oquevailevar':'Oficina de musica', 'quem':'Genezys'}
  // ]
});

tramaApp.controller('participacaoCtrl',function($scope){
  $scope.participacao=[
    {oquevailevar:"Oficina de malabares", quem:"Bernardo"},
    {oquevailevar:"Oficina de skate",quem:"Salazar"},
    {oquevailevar:"Oficina de teatro", quem:"Gabriela"},
    {oquevailevar:"Oficina de informatica", quem:"Genezys"},
    {oquevailevar:"Oficina de teatro", quem:"Gabriela"} 
  ];

  $scope.adicionarParticipacao = function(participa){
    $scope.participacao.push(angular.copy(participa));
    delete $scope.participa;
  };
});

tramaApp.controller('demandaCtrl',function($scope){
  $scope.listDemanda=[
    {oquedemanda:"#cachorroQuente"},
    {oquedemanda:"#malabares"},
    {oquedemanda:"#bandaDeRock"},
    {oquedemanda:"#artistasIndependentes"},
    {oquedemanda:"#contacaoDeHistoria"},
    {oquedemanda:"#oficinaDeStencil"},
    {oquedemanda:"#oficinaDeGrafite"},
    {oquedemanda:"#degustacaoDePaesArtesanais"}
  ];

  $scope.adicionarDemanda = function(demanda){
    $scope.listDemanda.push(angular.copy(demanda));
    delete $scope.demanda;
  };
});