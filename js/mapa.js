// Note: This example requires that you consent to location sharing when
// prompted by your browser. If you see the error "The Geolocation service
// failed.", it means you probably did not give permission for the browser to
// locate you.

function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -22.977755, lng: -43.234138},
    zoom: 6
  });

  setMarkers(map);
  var infoWindow = new google.maps.InfoWindow({map: map});

  // Try HTML5 geolocation.
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };

      infoWindow.setPosition(pos);
      infoWindow.setContent('Você está aqui!.');
      map.setCenter(pos);
    }, function() {
      handleLocationError(true, infoWindow, map.getCenter());
    });
  } else {
    // Browser doesn't support Geolocation
    handleLocationError(false, infoWindow, map.getCenter());
  }
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(browserHasGeolocation ?
                        'Error: The Geolocation service failed.' :
                        'Error: Your browser doesn\'t support geolocation.');
}

var casasColabs = [
  ['Base Coworking', -23.540254, -46.701933],
  ['The View Coworking', -23.596946, -46.646123],
  ['Iwork Broolkin', -23.627369, -46.700940],
  ['Coworking Sabiá', -23.600478, -46.665498],
  ['Pixels Coworking', -23.609041, -46.661319],
  ['Clube Nova Educa', -23.542397, -46.691065],
  ['Plug', -23.599508, -46.687666],
  ['Share Work Spot', -23.613854, -46.666188],
  ['Coworking, Casa Vermelha',-23.549262, -46.674876],
  ['Oficina São João',-23.541475, -46.641592],
  ['Corujas Coworking',-23.549973, -46.696013],
  ['Lab Fashion', -23.549318, -46.654092],
  ['Hilo Coworking', -23.557184, -46.688767],
  ['Hedra + AMP Coworking', -23.557611, -46.690396],
  ['VIP Office', -23.612257, -46.692576],
  ['FLEX IN Business Center', -23.629505, -46.694929],
  ['Gowork Coworking', -23.580250, -46.681824],
  ['Josefin Coworking', -23.559113, -46.668383],
  ['Clube De Negócios - Coworking e Networking', -23.594889, -46.633790],
  ['Casa Cause', -23.560049, -46.676400],
  ['Coworking.eco.br', -23.529829, -46.672739],
  ['Distrito', -23.566235, -46.669023],
  ['Estúdio 447',-23.599967, -46.665635],
  ['Add Group', -23.542801, -46.697888],
  ['Unicoworking',-23.562340, -46.690739],
  ['Oficina Coworking', -23.561161, -46.658906],
  ['TMZ HUB', -23.563251, -46.673855],
  ['WorkIn Office - Crescendo Juntos', -23.593035, -46.688502],
  ['WorkIn Office - Crescendo Juntos', -23.568464, -46.648551],
  ['WorkIn Office - Crescendo Juntos', -23.571273, -46.690256],
  ['Ivy Office', -23.580097, -46.657349],
  ['MYHUB Coworking', -23.502128, -46.620426],
  ['Coworking Consolação', -23.546844, -46.646271],
  ['Multi & Radar - Coworking', -23.601785, -46.614017],
  ['Osmose Coworking', -23.644394, -46.701397],
  ['Soma Escritório Compartilhado', -23.548512, -46.637506],
  ['Conjunto63', -23.544590, -46.640197],
  ['Casa de Viver', -23.592620, -46.635306],
  ['Salamandra', -23.633255, -46.689858],
  ['2Work Escritório Profissional', -23.568178, -46.649206],
  ['Mako Coworking', -23.584691, -46.647040],
  ['Delta Business Center', -23.570120, -46.690891],
  ['Delta Business Center', -23.566637, -46.649599],
  ['Mútua Coworking', -23.571117, -46.697846 ],
  ['Pocket Coworking', -23.532553, -46.666541],
  ['GP Coworking', -23.596988, -46.719124],
  ['Place2Work Vila Olímpica', -23.596669, -46.686129],
  ['Link2U Coworking & Offices', -23.557341, -46.660439],
  ['Cowave', -23.554001, -46.663420],
  ['BUCC | Workspaces', -23.554001, -46.663420],
  ['Espaço Eureka', -23.592925, -46.623578],
  ['LD78', -23.594927, -46.646432],
  ['VIP Office', -23.576773, -46.641383],
  ['Coworking Office Vila Olímpica', -23.596419, -46.687786],
  ['Coworking Master Jacaré', -23.547884, -46.753225],
  ['Boa Vista 21', -23.548102, -46.633755],
  ['Templo', -22.977755, -43.234138],
  ['Templo',-22.947678,-43.188249],
  ['EDX',-22.904923,-43.177971],
  ['Colworking Coworking',-22.896459,-43.122076],
  ['Circulo Coworking',-22.905430, -43.108656],
  ['My Oficce Escritórios Inteligentes',-22.984811,-43.218731],
  ['My Office Escritórios Inteligentes',-23.002803,-43.321700],
  ['Cluster Rio',-22.933020,-43.175428],
  ['Tribo Coworking',-22.972668,-43.188386],
  ['Porto Virtual',-22.897900, -43.179308],
  ['Corworking Arpoador',-22.982718,-43.194510],
  ['POP',-22962382,-43.217247],
  ['DOCA',-22.983295, -43.202216],
  ['Barra Cowork',-23.003170,-43.326671],
  ['Collective',-22.983324, -43.239269],
  ['Wecompany',-23.003886,-43.317732],
  ['Espaço GPS',-22.873361,-43.337672],
  ['Espaço para trabalhar',-22986721,-43.489550],
  ['Diamond Job',-22.821206,-43.311818],
  ['GO2 Coworking',-22.924325, -43.247717],
  ['Vinst',-23.020816, -43.488033],
  ['XXVinte',-22.965384, -43.220155],
  ['Rio Office',-23.021750,-43.462135],
  ['Mères',-22.988746,-43.359746],
  ['M2M Office',-22.828042,-43.323184],
  ['Sharing E.C.',-23.563508,-46.680933],
  ['Impact Hub',-23.553028,-46.656483],
  ['Conecta Epaço de Coworking',-23.207262,-45.897994],
  ['Bridge',-22.008330,-47.906623],
  ['Trampo S/A', -21.997460, -47.897420],
  ['Beehouse', -23.620847, -46.552965],
  ['Plug Office', -23.654607, -46.570407],
  ['Querido Odontologia', -23.626674, -46.663284],
  ['Locus Bussines', -23.967516, -46.324066],
  ['Espaço Certo', -23.945910, -46.329880],
  ['Caetan Escritorio', -22.409046, -47.580737],
  ['Global Hub', -21.207678, -47.764753],
  ['CoNéctar', -21.188337, -47.818633],
  ['Matrix Coworking', -24.010311, -46.413279]
];

function setMarkers(map) {
  var image = {
    url: 'img/coworkingMarker.png',
    // This marker is 20 pixels wide by 32 pixels high.
    size: new google.maps.Size(32, 37),
    // The origin for this image is (0, 0).
    origin: new google.maps.Point(0, 0),
    // The anchor for this image is the base of the flagpole at (0, 32).
    anchor: new google.maps.Point(16, 32)
  };

  for (var i = 0; i < casasColabs.length; i++) {
    var casa = casasColabs[i];
    var marker = new google.maps.Marker({
      position: {lat: casa[1], lng: casa[2]},
      map: map,
      icon:image,
      title: casa[0]
    });
  }
}